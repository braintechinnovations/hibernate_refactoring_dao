package corso.lez14.hibernate.model.crud;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import corso.lez14.hibernate.model.Persona;
import corso.lez14.hibernate.model.db.GestoreSessioni;

public class PersonaDAO implements Dao<Persona>{

	@Override
	public void insertion(Persona t) {

		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();			//1. Avvio una nuova transazione
			
			sessione.save(t);					//2. Effettuo tutte le operazioni che voglio
			
			sessione.getTransaction().commit();		//3. Se tutte le operazioni vanno a buon fine allora effettuo la Commit! Altrimenti, Rollback!
			
			System.out.println(t.toString());
			
		} catch (Exception errore) {
			System.out.println(errore.getMessage());
		} finally {
			sessione.close();						//Alla fine dell'utilizzo della sessione, chiudo!
			System.out.println("Connessione Chiusa!");
		}
	}

	@Override
	public boolean deletion(Persona t) {
		
		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
//			int affRows = sessione.createQuery("DELETE Persona WHERE id = 3").executeUpdate();
			Query sql = sessione.createQuery("DELETE Persona WHERE id = :valore");		//Equivalente di Prepared Statement
			sql.setInteger("valore", t.getPersonaId());
			
			int affRows = sql.executeUpdate();
			
			if(affRows > 0)
				return true;
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Connessione chiusa");
		}
		
		return false;
	}

	@Override
	public boolean deletion(int id) {
		
		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		try {
			
			sessione.beginTransaction();
			
//			int affRows = sessione.createQuery("DELETE Persona WHERE id = 3").executeUpdate();
			Query sql = sessione.createQuery("DELETE Persona WHERE id = :valore");		//Equivalente di Prepared Statement
			sql.setInteger("valore", id);
			
			int affRows = sql.executeUpdate();
			
			if(affRows > 0)
				return true;
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Connessione chiusa");
		}
		
		return false;
	}

	@Override
	public List<Persona> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
	//Update

	@Override
	public Persona findById(int id) {

		SessionFactory factory = GestoreSessioni.getGestore().getFactory();
		Session sessione = factory.getCurrentSession();
		
		Persona risultato = null;
		
		try {
			
			sessione.beginTransaction();
			
			risultato = sessione.get(Persona.class, id);
			
			sessione.getTransaction().commit();
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
			System.out.println("Connessione chiusa");
		}
		
		return risultato;
		
	}

}
