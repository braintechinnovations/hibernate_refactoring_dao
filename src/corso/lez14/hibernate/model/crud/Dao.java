package corso.lez14.hibernate.model.crud;

import java.util.List;

import corso.lez14.hibernate.model.Persona;

public interface Dao<T> {

	void insertion(T t);

	boolean deletion(T t);
	
	boolean deletion(int id);
	
	List<Persona> findAll();
	
	Persona findById(int id);
	
}
