package corso.lez14.hibernate.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity								//Definisco che questa classe � un'entit�
@Table(name="persona")				//Specifico a quale tabella devo collegarla
public class Persona {

	@Id								//L'attributo che segue � una Primary Key
	@GeneratedValue(strategy = GenerationType.IDENTITY)	//Si tratta di un campo che verr� generato in automatico seguendo la strategia IDENTITY (Mysql Auto Increment)
	@Column(name="personaId")
	private int id;
	
	@Column(name="per_nome")		//Specifico la colonna collegata a questo attributo
	private String nome;
	
	@Column(name="per_cognome")
	private String cognome;

	@Column(name="per_email")
	private String email;
	
	public Persona() {
		
	}

	public Persona(int id, String nome, String cognome, String email) {
		super();
		this.id = id;
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
	}
	
	public Persona(String nome, String cognome, String email) {
		this.nome = nome;
		this.cognome = cognome;
		this.email = email;
	}
	
	public int getPersonaId() {
		return id;
	}

	public void setPersonaId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "Persona [id=" + id + ", nome=" + nome + ", cognome=" + cognome + "]";
	}
	
	
	
}
