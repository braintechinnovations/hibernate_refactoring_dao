package corso.lez14.hibernate;

import corso.lez14.hibernate.model.Persona;
import corso.lez14.hibernate.model.crud.PersonaDAO;

public class Main {

	public static void main(String[] args) {

		Persona perUno = new Persona("Giovanni", "Pace", "giovanni@ciao.com");

		PersonaDAO daoPersona = new PersonaDAO();
		daoPersona.insertion(perUno);
		
//		if(daoPersona.deletion(1)) {									//Deletion tramite id
//			System.out.println("Operazione effettuata con successo");
//		}
//		else {
//			System.out.println("Problema di eliminazione");
//		}
		
//		System.out.println(daoPersona.findById(3));
		
		Persona daEliminare = daoPersona.findById(3);
		if(daoPersona.deletion(daEliminare)) {							//Deletion tramite Oggetto
			System.out.println("Operazione effettuata con successo");
		}
		else {
			System.out.println("Problema di eliminazione");
		}
	}

}
